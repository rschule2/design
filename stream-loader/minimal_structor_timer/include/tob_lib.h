/*  Header file for the TOB Library functions */

#ifndef TOB_LIB_H
#define TOB_LIB_H

#ifndef TRUE
  #define TRUE (1)
#endif

#ifndef FALSE
  #define FALSE (!TRUE)
#endif

/*
ANSI Colors
*/
#define SGR_F_BLACK   "\x1b[30m"
#define SGR_F_RED     "\x1b[31m"
#define SGR_F_GREEN   "\x1b[32m"
#define SGR_F_YELLOW  "\x1b[33m"
#define SGR_F_BLUE    "\x1b[34m"
#define SGR_F_MAGENTA "\x1b[35m"
#define SGR_F_CYAN    "\x1b[36m"
#define SGR_F_WHITE   "\x1b[37m"

#define SGR_B_BLACK   "\x1b[40m"
#define SGR_B_RED     "\x1b[41m"
#define SGR_B_GREEN   "\x1b[42m"
#define SGR_B_YELLOW  "\x1b[43m"
#define SGR_B_BLUE    "\x1b[44m"
#define SGR_B_MAGENTA "\x1b[45m"
#define SGR_B_CYAN    "\x1b[46m"
#define SGR_B_WHITE   "\x1b[47m"
#define SGR_RESET     "\x1b[0m"

// SOI Kerf test functions library

#define VERBOSITY_QUIET        (0x0000)
#define VERBOSITY_ERROR        (0x0001)
#define VERBOSITY_SUMMARY      (0x0002)
#define VERBOSITY_SUMMARY_TIME (0x0004)
#define VERBOSITY_FAILS        (0x0010)
#define VERBOSITY_STATUS       (0x0100)
#define VERBOSITY_FULL         (0x0800)
#define VERBOSITY_DEBUG        (0x8000)

/* Fixed sorts used by the control program (Also Must be in the sortinfo file) */

#define STARTING_SORT   (255)          // default starting sort
#define SORT_NONE        (-1)          // Used when no sort will be set on fail (Eng Test etc)

#define SORT_USBIO      (254)          // Tester usb io error
#define SORT_FILESYSTEM (253)          // Tester file system error
#define SORT_FILES      (252)          // Tester File Missing
#define SORT_ARGS       (251)          // Tester Program Arguments
#define SORT_PROGRAM    (250)          // Tester Program Error
#define SORT_HWSETUP    (249)          // Tester Hardware setup issue (Ambient Light too high etc)
#define SORT_HWERROR    (248)          // Tester Hardware error



#define TEST_INI_FILE "tester"



enum TEST_TIMERS { TEST_TIMER_1 = 1,
                   TEST_TIMER_2 = 2,
                   TEST_TIMER_3 = 3,
                   TEST_TIMER_4 = 4,
                   TEST_TIMER_5 = 5,
                   TEST_TIMER_6 = 6,
                   TEST_TIMER_7 = 7,
                   TEST_TIMER_8 = 8,
                   TEST_TIMER_9 = 9};



/* test control functions */
// Program command line flags
extern bool no_teds;                // flag to skip TEDS data logging
extern bool firstsite;
extern bool images;
extern bool profile_flag;
// extern bool pause_flag;             // Will stop at TestPause
extern bool continue_on_fail;
extern int  eng_datalog_mode;
extern bool eng_datalog_teradyne_flag;
extern bool csv_flag;
extern bool stdf_flag;
extern bool tty_out_flag;            // output is to a terminal (1 = not re-directed, 0 = redirected)
extern bool fail_flag;

extern bool run_start_of_lot_flag;
extern bool run_start_of_wafer_flag;
extern bool run_end_of_wafer_flag;
extern bool run_end_of_lot_flag;
extern bool run_board_cal_flag;

extern bool run_contact_only_flag;
extern bool run_contact_setup_flag;
extern bool run_production_setup_flag;
extern bool run_contact_flag;
extern bool noprober_flag;

extern char part_number[20];
extern char lot_number[20];
extern char lot_suffix[20];
extern char lot_test_date[25];
extern char wafer_id[20];
extern char site_xy[20];
extern char product_code[10];
extern char family_code[10];
extern int  wafer_slot;
extern double  test_temperature ;

extern char teds_filename_str[256];
extern char data_fname_pre[512];
extern char data_fname_partid[512];

extern char test_char;                     /* Character from the wafer map */

// extern char programname[1024];

extern int  tester_id;
extern char tester_id_str[80];             // for tester ID datalog
extern char inifile[200];







// IO functions
void PrintMessage(int vb, const char *fmtlist, ...);
void PrintMessageVerbosity(unsigned int vb);

void print_hex_data(const u_char *hex_data, int len);
void print_hex_data_device(FILE * outdev, const u_char *hex_data, int len);

#if 0
// Cage I/O routines
int CagePrintControllerInfo(void);
CardCage *CageOpen(void);
unsigned int CageReadReg(uint32_t slot, unsigned char regnum);
unsigned int CageWriteReg(uint32_t slot, unsigned char regnum, uint32_t regdata);
int CageClose(void);
#endif

int  TestPause(char msg[]);
void TestPauseEnable(bool pause_flag);
int  MsgPause(char msg[]);
void PrintPassFail(char * fname, int sortbin);


void TimerReset(void);
double TimerElapsed(void);
void TimerReset(TEST_TIMERS tnum);
double TimerElapsed(TEST_TIMERS tnum);



// SSRFT SMEM_funcs and data
// External access variables
// extern int    ssrft_smem_id;


int SsrftSmemOpen(void);
int SsrftSmemClose(void);
char *SsrftSmemGetTheadPtr(void);
void *SsrftSmemGetUserPtr(void);
const char *SsrftSmemGetVersionStr(void);              /* Returns a pointer to the version string */

const char *SsrftSmemGetWaferId(void);
uint32_t SsrftSmemGetWaferSlot(void);
const char *SsrftSmemGetWaferPartnumber(void);
const char *SsrftSmemGetLotNumber(void);
const char *SsrftSmemGetLotSuffix(void);
const char *SsrftSmemGetStartDate(void);
const char *SsrftSmemGetStartTime(void);
const char *SsrftSmemGetTestTemp(void);
uint32_t SsrftSmemGetTesterId(void);
const char *SsrftSmemGetTesterHostname(void);
uint32_t SsrftSmemGetFirstSiteFlag(void);
uint32_t SsrftSmemGetRunMode(void);
const char *SsrftSmemGetTedsIncFilename(void);
const char *SsrftSmemGetFuseFilename(void);
const char *SsrftSmemGetEcidFilename(void);

uint32_t SsrftSmemGetTestChar(uint16_t site);
uint32_t SsrftSmemGetWaferSiteX(uint16_t site);
uint32_t SsrftSmemGetModuleNumber(uint16_t site);
uint32_t SsrftSmemGetWaferSiteY(uint16_t site);
uint32_t SsrftSmemGetStartFlag(uint16_t site);
uint32_t SsrftSmemGetSampleFlag(uint16_t site);
uint32_t SsrftSmemGetSortBin(uint16_t site);
uint32_t SsrftSmemGetSoftBin(uint16_t site);
int32_t SsrftSmemSetSortBin(uint16_t site, uint16_t sort_bin);
int32_t SsrftSmemSetSortBin(uint16_t site, uint16_t sort_bin);
int32_t SsrftSmemSetSoftBin(uint16_t site, uint16_t sort_soft);


/* Command line parse functions */
int ParseArgs(int argc, char *argv[]);
int ParseArgCallback(char *arg1);

bool SsrftGetContactFlag(void);
bool SsrftGetCOFFlag(void);
const char *SsrftGetProgramName(void);
const char *SsrftGetTesterIdStr(void);
int SsrftGetTesterId(void);



void ShowHelp(void);
void ShowHelpCallback(void);



#endif


