// @file Structor_write_d2_i1.h (project structor-01)
// @date 2018.04.20
// @version (see git tags)
// @author schuler
// @info compile with: make

// @info Load a struct from file; file loader embedded in struct definition
// @info This use case is a modified version of example Structor_d2_i1.h
//       See Structor_d2_i1.h for detailed comments.

// @info Adheres to c++98 standard... sorry. (tobify)
// @info Developers search '@dev' for usage notes.

#ifndef STRUCTOR_D2_I1_H
#define STRUCTOR_D2_I1_H

#include <unistd.h>     // posix compliance
#include <iostream>     // stream io
#include <string>       // STL

#include "structor.h"   // structorSkipComment()

struct Structor_d2_i1 {
  // Example TOB struct
  // @dev: keep the ">>" operator in sync with the passive data members.
  double d_01;
  double d_02;
  int    i_01;

  // @dev data specific '>>' operator is required
  // --c++-- a class friend is a non-member with access to private data
  friend std::istream & operator >>(std::istream & in, 
				     Structor_d2_i1 & p) {
    in >> extendwhite 
       >> p.d_01 >> p.d_02 >> p.i_01 >> eol;

    return in;
  }

  friend std::ostream & operator <<(std::ostream & out, 
				     Structor_d2_i1 & p) {
    std::string dl(","); // comma delimiter
    out << p.d_01 << dl << p.d_02 << dl << p.i_01 << std::endl;
    return out;
  }

}; // end struct Structor_d2_i1

#endif // end STRUCTOR_D2_I1_H
