// @file Structor_ui64_2.h structor-01)
// @date 2018.04.16
// @version (see git tags)
// @author schuler
// @info compile with: make

// @info Load a struct from file; file loader embedded in struct definition

// @info This use case is a modified version of example Structor_d2_i1.h
//       See Structor_d2_i1.h for detailed comments.

// @info Adheres to c++98 standard... sorry. (tobify)
// @info Developers search '@dev' for usage notes.

#ifndef STRUCTOR_UI64_H
#define STRUCTOR_UI64_H

#include <unistd.h>     // posix compliance
#include <iostream>     // stream io
#include <string>       // STL
#include <stdint.h>       
#include <iomanip>      // save/restore stream manipulators; copyfmt()

#include "structor.h"   // structorSkipComment()

struct Structor_ui64_2 {
  // Example TOB struct
  // @dev: keep the ">>" operator in sync with the passive data members.
  uint64_t ui64_01;
  uint64_t ui64_02;

  // @dev data specific '>>' operator is required
  friend std::istream & operator >>(std::istream & in, 
				     Structor_ui64_2 & p) {
    // load passives; note data are formated as hex in ascii file
    // static ensures single allocation
    static std::ios sstate(NULL); sstate.copyfmt(std::cout); // save stream state

    in >> extendwhite >> std::hex >> p.ui64_01 >> p.ui64_02 >> eol;

    std::cout.copyfmt(sstate); // replace stream state
    return in;
  }
}; // end struct Structor_ui64_2

#endif // end STRUCTOR_UI64_H
