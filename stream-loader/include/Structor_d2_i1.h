// @file Structor_d2_i1.h (project structor-01)
// @date 2018.04.13
// @version (see git tags)
// @author schuler
// @info compile with: make

// @info Load a struct from file; file loader embedded in struct definition
// @info Naming convention: <StructName>_Tn.h
//       StructName should match the struct definition
//       Tn represents the type and number of passive data elements
//       e.g. Structor_d2_i1 implies a named struct 
//       with 2 doubles and 1 int

// @date 2018.04.16 rcs change naming convention to Structor_d1_i1.h/cpp
// @date 2018.04.25 rcs add eor (\n) requirement; 'eol' manipulator

// @info Adheres to c++98 standard... sorry. (tobify)
// @info Developers search '@dev' for usage notes.

#ifndef STRUCTOR_D2_I1_H
#define STRUCTOR_D2_I1_H

#include <unistd.h>     // posix compliance
#include <iostream>     // stream io
#include <string>       // STL

#include "structor.h"   // extendwhite, eol

struct Structor_d2_i1 {
  // Example TOB struct
  // The passive data members and 'operator' are customzed to specific
  // datasets.  Although csv files ( comma delimiter ) is most common,
  // any char may be mapped into a white space.
  //
  // Ignores comment lines.  Comment lines are defined as lines in the
  // ascii file with a '#' or '/' in the first (zeroth) column and 
  // ending with a standard linux newline '\n'.
  // 
  // Expect POSIX complient ascii data files; if input file has been created with 
  // prehistoric DOS tools, the file may be pre-processed with 
  // 'dos2unix', 'tr', etc..
  // 
  // Return value: '>>" operator sets standard iostream status flags.
  //               Caller expected to test this for bad data.
  //
  // --todo-- Make a version using 'class' in place of 'struct' for C++ coders
  // --todo-- Incorporate RFC feedback; deadline 2018.04.16
  // --todo-- Verify other data types ("strings", 'c'hars, ...)

  // @dev: keep the ">>" operator in sync with the passive data members.
  double d_01;
  double d_02;
  int    i_01;

  // @dev data specific '>>' operator is required
  // --c++-- a class friend is a non-member with access to private data
  // --rcs-- change iostream to istream
  // friend std::iostream & operator >>(std::iostream & in, 
  //				     Structor_d2_i1 & p) {
  friend std::istream & operator >>(std::istream & in, 
				     Structor_d2_i1 & p) {
    in >> extendwhite 
       >> p.d_01 >> p.d_02 >> p.i_01 >> eol;

    return in;
  }
}; // end struct Structor_d2_i1

#endif // end STRUCTOR_D2_I1_H
