// @file structor.h (project structor-01)
// @date 2018.04.13
// @version (see git log and tags)
// @author schuler
// @info header common to all structors; see structor.cpp

// @info Load a struct from stream pointer with overloaded i/o.

// @info Adheres to c++98 standard... sorry. (tobify)
// @info Developers search '@dev' for usage notes.

// @date 2018.04.25 rcs removed '\n' from ctype_base::space table
// @date 2018.04.27 rcs added 'eol' stream manipulator

#ifndef STRUCTOR_H
#define STRUCTOR_H

#include <unistd.h>     // posix compliance
#include <iostream>     // stream io
#include <string>       // STL
#include <locale>       // std::locale, imbue(), custom_ws()

// @dev data specific custom_ws struct is required
struct StructorTableWs : std::ctype<char> { 
  // Custom whitespace generator allows adding chars to the standard
  // whitespace definition ( space, tab, etc...)
  // 'newline' (\n) is required; unlike getline(), sstream ops do not 
  // eat 'newline'.  Could support windows format files by adding
  // '\r'; best to not do this to enforce order on software designers.
  // --todo-- make this list of white space runtime configuralbe
 StructorTableWs() : std::ctype<char>(get_table()) {}
  static mask const* get_table()
  {
    static mask rc[table_size];
    rc[','] = std::ctype_base::space;
    rc[' '] = std::ctype_base::space;
    rc[';'] = std::ctype_base::space;
    rc[':'] = std::ctype_base::space;
    return &rc[0];
  } // end static mask const* get_table()
}; // end struct StructorTableWs : std::ctype<char> 

void structorStreamConfig( std::iostream & in ); // --depricated--

std::istream &extendwhite(std::istream &in);
std::istream &eol(std::istream &in); // test for unix eol ('\n')

#endif // end STRUCTOR_H
