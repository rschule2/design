// @file Structor_s1_ui32_6.h structor-01)
// @date 2018.04.16
// @version (see git tags)
// @author schuler
// @info compile with: make

// @info Load a struct from file; file loader embedded in struct definition

// @info This use case is a modified version of example Structor_d2_i1.h
//       See Structor_d2_i1.h for detailed comments.

// @info Adheres to c++98 standard... sorry. (tobify)
// @info Developers search '@dev' for usage notes.

#ifndef STRUCTOR_S1_UI32_6_H
#define STRUCTOR_S1_UI32_6_H

#include <unistd.h>     // posix compliance
#include <iostream>     // stream io
#include <string>       // STL
#include <stdint.h>       
#include <iomanip>      // save/restore stream manipulators; copyfmt()

#include "structor.h"   // structorSkipComment()

struct Structor_s1_ui32_6 {
  // Example TOB struct
  // @dev: keep the ">>" operator in sync with the passive data members.
  std::string s_01;
  uint32_t ui32_01;
  uint32_t ui32_02;
  uint32_t ui32_03;
  uint32_t ui32_04;
  uint32_t ui32_05;
  uint32_t ui32_06;
  
  // @dev data specific '>>' operator is required
  friend std::istream & operator >>(std::istream & in, 
				     Structor_s1_ui32_6 & p) {
    // load passives; note ui32_3-6 are formated as hex in ascii file
    // static ensures single allocation
    static std::ios sstate(NULL); sstate.copyfmt(std::cout); // save stream state
    
    in >> extendwhite >> p.s_01  
       >>  p.ui32_01  >> p.ui32_02 
       >>  std::hex >> p.ui32_03 >> p.ui32_03 >> p.ui32_05 >> p.ui32_06
       >> eol;

    std::cout.copyfmt(sstate); // replace stream state
    return in;
  }
}; // end struct Structor_s1_ui32_6

#endif // end STRUCTOR_S1_UI32_6_H
