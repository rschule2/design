// @file Structor_d3_i1_s1.h (project structor-01)
// @date 2018.06.05
// @version (see git tags)
// @author schuler
// @info compile with: make

// @info Load a struct from file; file loader embedded in struct definition
// @info Naming convention: <StructName>_Tn.h
//       StructName should match the struct definition
//       Tn represents the type and number of passive data elements
//       e.g. Structor_d3_i1_s1 implies a named struct 
//       with 2 doubles and 1 int

// @date 2018.06.05 rcs from Structor_d1_i1.h; test suspect case d2_i1_s1a

// @info Adheres to c++98 standard... sorry. (tobify)
// @info Developers search '@dev' for usage notes.

#ifndef STRUCTOR_D3_I1_S1_H
#define STRUCTOR_D3_I1_S1_H

#include <unistd.h>     // posix compliance
#include <iostream>     // stream io
#include <string>       // STL

#include "structor.h"   // extendwhite, eol

struct Structor_d3_i1_s1 {
  // @dev: keep the ">>" operator in sync with the passive data members.
  double d_01;
  double d_02;
  double d_03;
  int    i_01;
  std::string s_01;

  // @dev data specific '>>' operator is required
  // --c++-- a class friend is a non-member with access to private data
  // --rcs-- change iostream to istream
  // friend std::iostream & operator >>(std::iostream & in, 
  //				     Structor_d3_i1_s1 & p) {
  friend std::istream & operator >>(std::istream & in, 
				     Structor_d3_i1_s1 & p) {
    in >> extendwhite 
       >> p.d_01 >> p.d_02 >> p.d_03 >> p.i_01 >> p.s_01 >> eol;

    //    >> p.d_01 >> p.d_02 >> p.d_03 >> p.i_01 >> p.s_01 >> eol;

    return in;
  }
}; // end struct Structor_d3_i1_s1

#endif // end STRUCTOR_D3_I1_S1_H
