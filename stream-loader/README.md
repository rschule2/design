File: stuctor/README.md
Date: 2018.04.27
Author: schuler
@info stream-loader additional info; see also code headers and changelog.txt

--------------------------------------------------------------------------------
Note: This version of 'stream-loader' is for use a coding sample only.
This project normaly controlled by an encryped git repo. The .git
directory, with historical commits, has been removed for this sample code.
-- Ray Schuler 2018.08.01
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
Project 'stream-loader' is a generic method for reading and writing delimited 
ascii data from character streams, such as files and network sockets, from 
and to a struct or class.  I define the object combining passive data and 
i/o operators ">>" and "<<" to be a 'structor'.

a stream-loader structor may use the typical csv delimiter, ',', space
delimited data, ' ', and the whitespace table is also extended to use
semi-colon, ';', and colon, ':', delimited data.

The following standard data structure 'StandardStruct_d2_i1' is replaced
with the structor 'Structor_d2_i1'.  The operator overload of ">>"
provides the means for loading a delimited ascii files dataset into
the structor.  A similar friend "<<" overload is included when
file writes are required.

struct StandardStruct_d2_i1 {  // non structor container
  double d_01;
  double d_02;
  int    i_01;
}; 

struct Structor_d2_i1 {        // structor container

  double d_01; 
  double d_02;
  int    i_01;

  friend std::iostream & operator >>(std::iostream & in, 
				     Structor_d2_i1 & p) {
    in >> extendwhite 
       >> p.d_01 >> p.d_02 >> p.i_01 >> eol;
    return in;
  }
}; 

Assuming 'fin' is file stream open for reading, structors sets of
any length may be loaded with a loop construct such as:

while (fin >> structor_d2_i1) { ...)

Simllarly, structor sets may be written to an output file stream with:

while (fout << structor_d2_i1) { ...)

The executables built in makefile are for example usage and regression
testing.  'go.sh' and 'go.sh.all' are bash scripts to run all regression
tests.  'go.sh.out' contains the expected output from 'go.sh.all'.

e.g. execute '. ./go.sh.all > go.sh.new.out' and
'diff go.sh.new.out go.sh.out' for a quick regression test.

In addition to this README, see 'minimal_structor', and other examples.

--------------------------------------------------------------------------------
@info Requirements:
--------------------------------------------------------------------------------

- Load unix format ascii records with character delimited fields into 
  a header file defined struct.

  Structors provide support for limits files, pattern files, spi vectors, etc...
  Input streams are most often files, though any character stream (such as
  network sockets) are supported as well.

- Data files must be POSIX complient ascii format; DOS format files are rejected.
  See 'Design Details' below for conversion info.

- Data files are delimited by an extended set of white space chars: 
    [ ',' ' ' ';' ':' ]

  A future release may include configuration files and/or command line
  arguemnts to reduce or augment the delimiter set.

- Supported data types:
  std::string, int, double, uint32, (hex)uint32, uint64, (hex)unit64

  These types are included in regression tests; other types are supported
  and will be added the list after regression tests are created.

  See 'Design Details' below.

- String data fields may not contain delimiter chars.

  A future release will support delimiters embedded in string fields
  encapsulated by double quotes.  See 'Design Details' for a discussion
  of performance issues associated with this feature.

- Comment lines in the data are supported and defined as lines beginning 
  with '#' or '/'.  

- Memory is dynamically allocated; data set size is limited only by 
  user quotas and virtual memory space.

- Structors may optionally embed a write method, including the choice
  of field delimiter.

--------------------------------------------------------------------------------
@todo RFC Requests:
--------------------------------------------------------------------------------
     Complete:
      - add profiling timers to use case example (john) (complete 2018.04.18)
      - create use case on specific pattern file (becca)
        (complete 2018.04.17)
      - scalability; parametrize structors; perhaps pre-processing (calvin)
        (scalability complete 2018.04.13; pre-processing unnecessary)
      - replace 'config' call with stream modifier (rcs) (complete 2018.04.19)
      - bug: end of file comment hangs (ritin) (fixed 2018.04.18)
      - add structor file writer (rcs) (complete 2018.04.22)
      - code allows comments after data; I should kill this 
        (calvin) (complete 2018.04.25)
      - simplify newline get() with eol manipulator (rcs) (complete 2018.04.27)
      - use istream/ostream in place of iostream to generate compile time
        error on user misuse (stewart) (complete 2018.05.21)
      - rm *.[oa] from .gitignore; track *.o/.a (stewart) (complete 2018.05.23)

     Pending:
      - look for alternatives to try/catch (calvin)
      - discuss c++ exception stack tracing with Gunther (calvin/rcs)
      - address performance hit allowing delimeters in quoted strings (rcs)
      - create a piped i/o executable example (rcs)
      - alternative solution to trailing delimiter for std::string as last
        field case (stewart)
      - removed 'static' from extendwhite() NLS table object. 'new()' is 
        now called for each extendwhite() call.  Need a technique to 
	free this memory.  'static' causes problems when >>extendwhite
        called from a class constructor (and proably any class method?)
	(stewart/ray)

        
--------------------------------------------------------------------------------
@info cloning the master repository:
--------------------------------------------------------------------------------
(removed for this sample version)


For full details on usage development, see REDME.md and changelog.txt


Update 2018.07.23
The build variable 'CFLAGS' in makefile now defaults to a modern
64bit OS target.  To compile on a legacy 32bit OS, modify makefile
to read as follows:

# For modern OS, remove -m32 (or install 32 bit dependencies)
# CFLAGS = -g -Wall -L$(LIB_DIR) -I$(INC_DIR) -lstructor

# TOB (centos6) CFLAGS as of 2018.04.25
CFLAGS = -g -m32 -Wall -L$(LIB_DIR) -I$(INC_DIR) -lstructor

--------------------------------------------------------------------------------
File Descriptions:
--------------------------------------------------------------------------------

./structor                         // project directory
./README
./makefile
./src
./include
./go.sh.all                        // regression script
./go.sh.out                        // regression script expected results

./src/structor.h                   // structor core header; required by user code
./lib/libstructor.a		   // structor core lib; required by user code

./src/structor.cpp                 // structor core source code; not required
				   // by end users; use 'libstructor.a' instead

./include/Structor_d2_i1.h         // model use case; use Structor_d2_i1.* as
./src/Structor_d2_i1.cpp           // the cleanest example.  Extra examples, 
./Structor_d2_i1.txt	           // including 'structor_minimal*', may not
				   // be as well formatted as Structor_d2_i1.*

./minimal_structor		   // absolute minimum stand alone use case
./minimal_structor_timer	   // see 'makefile' for linking 'libstructor.a'

./include/Structor_write_d1_21.h   // example use case - read and write
./src/Structor_write_d2_i1.cpp     // example use case - read and write

./include/Structor_s1_ui32_6.h     // example use case - pattern file
./src/Structor_s1_ui32_6.cpp       // example use case - pattern file
./Structor_s1_ui32_6.txt	   // example csv-like data set - pattern file

./include/Structor_ui64_2.h        // example use case - spi file
./src/Structor_ui64_2.cpp          // example use case - spi file
./Structor_ui64_2.txt	           // example csv-like data set - spi file

--------------------------------------------------------------------------------
Design Details:
--------------------------------------------------------------------------------

Data files must be POSIX compliant; the primary target is OS is Linux.
If data files are created from another OS (e.g. osx, dos, ..)
convert the files to a unix format. e.g. to convert dos(windows) to unix:

  tr -d '\r' < input.file > output.file

Structors not currently (2018.04.16) handle delimiter characters in string fields
surrounded by double quotes.  This can be added if requested. If implimented,
a pre-compile macros flag will be added; embeded delimiters should be a 
special case since they will degrade performance.

When the final field of a record is of type std::string, that text file entry
must be followed by a valid delimiter, e.g. ';'.  Without this trailing 
delimiter, the EOL (\n) char is considered part of the string.  If you are
strongly opposed to the trailing delimiter, re-order the fields such that 
the last field in a numeric type. See test case data/Structor_d3_i1_s1.txt.

Comment lines in the data are supported and defined as lines beginning 
with '#' or '/'.  Multiple comment lines are allowed throughout the data set.

A future release may allow comment trigger customization; for simplicity
and reliability comma delimeters anre pound sign comment triggers
are encouraged unless customization is required by the application.

Data file records may include integer fields in decimal or hex format.
If (hex) formatting is used, e.g. 0xFFFF, the standard iostream 
manipulator must be included int he ">>" overload function.  The
overloated input stream operator is alwasy present with the structor
struct definition.

By convention, the user is encouraged to include a data comment line
specifing the file format.  In words, the second comment line in the
example below states, "Each record contains two fields of type unsigned
64 bit integer, given in hexadecimal notation."

# RegAddr, RegVal(uint64_t)
# format: (hex)uint64 x 2
0x0000,0x0000000143e0
0x0001,0xf00000000000
...

Example invocation and output of the prototype code:

$ ./structor testfile.txt
Contents of structor loaded line by line:
d_01: 1.1	d_02: 2.2	i_01: 1
d_01: 10.1	d_02: 20.2	i_01: 10
d_01: 100.1	d_02: 200.2	i_01: 100

Contents of v_structor in memory:
v_tob_struct[0]: 1.1	2.2	1	
v_tob_struct[1]: 10.1	20.2	10	
v_tob_struct[2]: 100.1	200.2	100
*/

/* 
   usage: ./structor <infile> 
   see @notes
*/

--------------------------------------------------------------------------------
@notes
--------------------------------------------------------------------------------

/*
- Code lines tagged with --debug-- are intended for removal (commenting)
  in final release.
- Adheres to c++98 standard for compatiblity with old OS builds.
  To install build on a modern OS (mint 18), remove the -m32 flag
  from makefile -> CFLAGS
- A 'class' version will be created for C++ devs.

*/