#!/bin/bash
# structor dev/regression testing

runtest() {
    cmd="${1}" # e.g. cmd="./structor_d2_i1 data/Structor_d2_i1.txt"
    echo "##" ${cmd}; ${cmd}
}

# special cases
# runtest "./structor_d2_i1 data/Structor_d2_i1-nocomment.txt"
# runtest "./structor_d2_i1 data/Structor_d2_i1_error_trailing_comment.txt"
# runtest "./structor_d2_i1 data/Structor_d2_i1-hand_edit.txt"
runtest "./structor_d3_i1_s1 data/Structor_d3_i1_s1.txt"

# normal conditions
# runtest "./structor_d2_i1 data/Structor_d2_i1.txt"
# runtest "./structor_s1_ui32_6 data/Structor_s1_ui32_6.txt"
# runtest "./structor_s1_ui32_6 requirements/patt_testfile.csv"
# runtest "./structor_ui64_2 data/Structor_ui64_2.txt"
# runtest "./structor_ui64_2 requirements/spi_testfile.csv"
# runtest "./structor_write_d2_i1 data/Structor_d2_i1.txt data/Structor_d2_i1.out"

# error conditions
# runtest "./structor_d2_i1 data/Structor_d2_i1_error_too_few_fields.txt"
# runtest "./structor_d2_i1 data/Structor_d2_i1_error_too_many_fields.txt"
# runtest "./structor_d2_i1 data/Structor_d2_i1_error_int_as_float.txt"

