// @file Structor_d2_i1.cpp (project structor-01)
// @date 2018.04.20
// @version (see git tags)
// @author schuler
// @info compile with: make

// @info Load a struct from file; file loader embedded in struct definition.
// @info Write output to a separate file.

// @info Adheres to c++98 standard... sorry. (tobify)
// @info Developers search '@dev' for usage notes.

// @info example output: Data set specific; load and display tabular file
/* 
  see go.sh.out
*/

/* 
   usage: ./structor_write_d2_i1 <infile> <outfile>
   see go.sh.out; see @notes; 
*/

#include <unistd.h>     // posix compliance
#include <iostream>     // stream io
#include <fstream>
#include <string>       // STL
#include <vector>
#include <cstdlib>      // EXIT_SUCCESS, EXIT_FAILURE
#include <stdexcept>    // std::runtime_error()

#include "Structor_write_d2_i1.h"
#include "structor.h"

int main( int argc, char* argv[] ) 
{
  try {
    // '__cplusplus' should report the c++ standard in use, but is broken 
    // on the obsolete TOB version of gcc/g++.  This bug was addressed in 2001.
    // On the TOB (Centos6/gcc 4.4.7), __cplusplus eroneously returns '1'.
    // 
    // std::cerr << __cplusplus << std::endl;  // show compiler standard used
    // // g++ -std=c++11 (or 14) for C++2011 (or 2014) to test

    if (argc != 3) { // usage: ./structor_* <input_file_name> <output_file_name>
      // Generic error handler; see also catch(*)
      // Build string 's' with desired detail; use this construct
      // througout such that 'catch(*)' is the only exception exit point
      // ex: "--Error-- ./structor: main: Process invoked without... etc..."
      std::string s = std::string(argv[0]) + std::string(": ")
	+ std::string(__FUNCTION__) + std::string(": ") 
	+ std::string("Process invoked without input and output file names.")
	+ std::string("\n\nUsage: ") + std::string(std::string(argv[0]))
	+ std::string(" <input_file_name> <output_file_name>\n");
      throw std::runtime_error(s.c_str());
    }

    // Reguired input file passed through command line 
    std::string s_fname = std::string(argv[1]);
    std::string s_fname_out = std::string(argv[2]);

    // Structor declaration
    // Initialize passive data members; do *not* re-initialize nl:=newline char
    // 'nl' assignment may only occur in *.h definition
    Structor_d2_i1 structor_d2_i1 = { // see Structor_d2_i1.h
      -1.0,  // d_01
      -2.0,  // d_02
      -3,    // i_01
    };
    
    // std::cout << "--debug--" << std::endl;

    // Open file for reading 
    std::fstream fin (s_fname.c_str(), std::ios::in);
    if (!fin) {
      // Generic error handler (see catch())
      // ex: "--Error-- ./structor: main: Required file not found: tmp.txt"
      std::string s = std::string(__FUNCTION__)
	+ std::string(": ")
	+ std::string("Required file not found: ") 
	+ std::string(s_fname);
      throw std::runtime_error(s.c_str());
    }

    // Open file for writing
    std::fstream fout (s_fname_out.c_str(), std::ios::out);
    if (!fout) {
      // Generic error handler (see catch())
      // ex: "--Error-- ./structor: main: Required file not found: tmp.txt"
      std::string s = std::string(__FUNCTION__)
	+ std::string(": ")
	+ std::string("Unable to open file: ")
	+ std::string(s_fname_out);
      throw std::runtime_error(s.c_str());
    }

    // Optionally create a container when placing the entire file in memory
    // is desired.
    std::vector<Structor_d2_i1> v_structor_d2_i1;

    // Core struct loading block; target application specific; 
    // Simple screen dump in this demo.
    std::cout << "Contents of structor_d2_i1 loaded line by line:\n";
    while (fin >> structor_d2_i1) {
      std::cout << "d_01: " << structor_d2_i1.d_01 << "\t";
      std::cout << "d_02: " << structor_d2_i1.d_02 << "\t";
      std::cout << "i_01: " << structor_d2_i1.i_01 << std::endl;
      v_structor_d2_i1.push_back(structor_d2_i1); // load entire file in memory
    }
    std::cout << std::endl;

    { // Dump the contents of vector container to stdout
      std::cout << "Contents of v_structor_d2_i1 in memory:\n";
      std::vector<Structor_d2_i1>::iterator it;  
      int i = 0;
      for ( it = v_structor_d2_i1.begin();
	    it != v_structor_d2_i1.end(); 
	    it++, i++ ) {
	std::cout << "v_structor_d2_i1[" << i << "]: ";
	std::cout << it->d_01 << "\t";
	std::cout << it->d_02 << "\t";
	std::cout << it->i_01 << "\t";
	std::cout << std::endl;
      }  // end for ( it =
      std::cout << std::endl;
    } // end Dump the contents of vector container to stdout
    
    { // Write structor vector to file
      std::cout << "Writing structor_d2_i1 to file " << s_fname_out <<std::endl;
      std::vector<Structor_d2_i1>::iterator it;  
      for ( it = v_structor_d2_i1.begin();
	    it != v_structor_d2_i1.end(); 
	    it++ ) {
	fout << *it;
      }  // end for ( it =
      std::cout << std::endl;
      // Dump file contents to stdout
      std::string s; s = "cat " + s_fname_out;
      std::cout << "Verify: " << s << std::endl;
      system(s.c_str());
    } // end Write structor vector to file
    
    // Return status checking
    // Sanity checking the returned data is left to the user.
    // if (fin.eof()) std::cout << "--debug-- eof() OK" << std::endl; 
    if (!fin.eof()) {
      // Generic error handler (see catch())
      // ex: "--Error-- ./structor: main: File read ended ...; bad data: tmp.txt"
      std::string s = std::string(__FUNCTION__)
	+ std::string(": ")
	+ std::string("File read ended prematurely; data/structor mismatch: ") 
	+ std::string(s_fname);
      throw std::runtime_error(s.c_str());
    } 

    exit (EXIT_SUCCESS);
    
  } // end try

  // Error handling
  catch(std::exception &e){
    std::cout << "--Error-- " << e.what() << std::endl;
    exit (EXIT_FAILURE);
  }
  catch(...){
    std::cerr << "--Error-- Undefined runtime error" << std::endl;
    exit (EXIT_FAILURE);
  };
  
  
} // end  int main( int argc, char* argv[] ) 

// Notes:
/*
  Code lines tagged with --debug-- are intended for removal (commenting)
  in final release.

  See minimal example Structor_d2_i1.cpp

 */
