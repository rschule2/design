// @file structor.cpp (project structor-01)
// @date 2018.04.13
// @version (see git log and tags)
// @author schuler
// @info Create structures with embedded stream read/write overload operator

// @info compile with: make
// @info Adheres to c++98 standard... sorry. (tobify)

// @date 2018.04.16 rcs fixed contiguous comment line bug; added loop
// @date 2018.04.18 rcs fixed comment after last data bug; check in.good()
// @date 2018.04.19 rcs replaced structorStreamConfig with manipulator extendwhite
// @date 2018.04.25 rcs added eor requirement (in structor.h); 'eol' manip;
//                      this fixed 'too few data fields' bug
// @date 2018.07.11 rcs removed 'static' for extendwhite StructorTableWs
//                      causing problems in calls from class; does this
//                      need to be freed?
 
#include <unistd.h>     // posix compliance
#include <iostream>     // STL
#include <locale>       // std::locale, imbue()
#include <iomanip>      // stream manipulators
#include <ios>          // setstate()

#include "structor.h"   

// structorStreamConfig is depricated; delete after 04/2018
void structorStreamConfig( std::iostream & in ) {
  // --deprecated-- use stream manipulator 'extendwhite'
  std::cerr << "--deprecated-- use '>> extendwhite' manipulator insead\n" 
	    << "--deprecated-- structorStreamConfig() will go away after 04/2018"
	    << std::endl;
  // Extend table of white space chars, and ignore comment lines

  // Extend white space char set (see structor.h)
  // Static prevents multiple table updates
  static StructorTableWs *pSTWS = new StructorTableWs;
  in.imbue(std::locale(in.getloc(), pSTWS)); 
  //   in.imbue(std::locale(in.getloc(), new StructorTableWs)); 

  // ignore comment lines
  char c;
  std::string s;
  in >> c; // These chars define a comment line
  // std::cout << "--debug-- in >> c: " << c << std::endl;
  while ( ((c == '#') || (c == '/')) && (in.good()) ) { // --2018.04.16--
    getline( in, s);
    // std::cout << "--debug-- comment string: " << s << std::endl;
    if (in.good()) { // --2018.04.16--
      in >> c; // is next char comment line start?
    }
  } // end while (
  if (in.good()) { // --2018.04.16--
    in.putback(c); // last char pulled was not comment start char
    //  std::cout << "--debug-- putback(c): " << c << std::endl;
  }
} // end void structorStreamConfig( std::iostream & in ) 
// structorStreamConfig is depricated; delete 04/2018

std::istream &extendwhite(std::istream &in) {
  // Extend table of white space chars, and ignore comment lines.
  // Coment lines are effectively 'white space' in the context of
  // loading streams into data structures.

  // --todo-- Consider processing delimiters within strings here.
  // --todo-- Get all lines and pre-process data; compiler macro
  // --todo-- switch, as this will have performance implications

  // extend white space char set (see structor.h)
  // static prevents multiple table updates
  // static StructorTableWs *pSTWS = new StructorTableWs;
  // --todo- removed 'static'; does this need to be freed?
  StructorTableWs *pSTWS = new StructorTableWs;
  in.imbue(std::locale(in.getloc(), pSTWS)); 

  // define and ignore comment lines
  char c;
  std::string s;
  in >> c; // chars below define start of a comment line
  // std::cout << "--debug-- in >> c: " << c << std::endl;
  while ( ((c == '#') || (c == '/')) && (in.good()) ) { // --2018.04.16--
    getline( in, s);
    // std::cout << "--debug-- comment string: " << s << std::endl;
    if (in.good()) { // --2018.04.16--
      in >> c; // is next char comment line start?
    }
  }
  if (in.good()) { // --2018.04.16--
    in.putback(c); // last char pulled was not a comment char
    // std::cout << "--debug-- putback(c): " << c << std::endl;
  }

  return in;
} // end std::istream &extendwhite(std::istream &in)

std::istream &eol(std::istream &in) {
  // expect unix eol char 
  #define NEWLINE '\n'

  std::istream::sentry s(in);
  if (s) while (in.good()) {
      char c = in.get();
      if (c == NEWLINE) 
	{ break;  // good; found expected eor
	} else 
	{
	  in.setstate(std::ios::failbit);
	}
    }
    return in;

} // end std::istream &eol(std::istream &in)

// @notes
/*
-----
// --dev-- simple example ostream manipulator; set stream to sci, precison 4
std::ostream &scientific(std::ostream &Out)
{
  Out << setiosflags(std::ios::showpoint | std::ios::scientific);
  Out << std::setprecision(4);
  return(Out);
}
// --dev-- use: 
std::cout << scientific << 3.14 << std::endl;

-----
// Early alternatives in Structor_d2_i1.h for eor chedk
    //    if ( in.get() != p.nl ) in.setstate(std::ios::failbit);
    // in.ignore(1, p.nl); // 1 for unix

-----

 */
