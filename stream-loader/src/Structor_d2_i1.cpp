// @file Structor_d2_i1.cpp (project structor-01)
// @date 2018.04.10
// @version (see git log, tags, and changelog)
// @author schuler
// @info compile with: make

// @date 2018.04.12 rcs separated StructorExample_d2i1 structor.cpp
//                      file as examples/structor_d2i1.cpp
// @date 2018.04.16 rcs naming convention change, Structor_d2_i1.h/cpp
// @date 2018.04.19 rcs replace sturctorStreamconfig with extendwhite manipulator
// @date 2018.04.25 rcs added eor requirement (end of record; e.g. nl:='\n')

// @info Load a struct from stream pointer; loader embedded in struct definition

// @info Adheres to c++98 standard... sorry. (tobify)

// @info example output: Data set specific; load and display tabular file
/* 
[tobtest@t8080b structor]$ ./structor testfile.txt
Contents of structor_d2_i1 loaded line by line:
d_01: 1.1	d_02: 2.2	i_01: 1
d_01: 10.1	d_02: 20.2	i_01: 10
d_01: 100.1	d_02: 200.2	i_01: 100

Contents of v_structor_d2_i1 in memory:
v_structor_d2_i1[0]: 1.1	2.2	1	
v_structor_d2_i1[1]: 10.1	20.2	10	
v_structor_d2_i1[2]: 100.1	200.2	100
*/

/* 
   usage: ./structor_d2_i1 <infile> 
   see @notes
*/

#include <unistd.h>     // posix compliance
#include <iostream>     // stream io
#include <fstream>
#include <string>       // STL
#include <vector>
#include <cstdlib>      // EXIT_SUCCESS, EXIT_FAILURE
#include <stdexcept>    // std::runtime_error()

#include "Structor_d2_i1.h"
#include "structor.h"

 // whereami() macro can be assigned to a string to report current
 // process name and function.  e.g. std::string s = whereami();
#define whereami() (std::string(argv[0]) + std::string(": ")		\
		    + std::string(__FUNCTION__) + std::string(":\n\t") )

int main( int argc, char* argv[] ) 
{
  try {
    // '__cplusplus' should report the c++ standard in use, but is broken 
    // on the obsolete TOB version of gcc/g++.  This bug was addressed in 2001.
    // On the TOB (Centos6/gcc 4.4.7), __cplusplus eroneously returns '1'.
    // 
    // std::cerr << __cplusplus << std::endl;  // show compiler standard used
    // // g++ -std=c++11 (or 14) for C++2011 (or 2014) to test


    if (argc != 2) { // usage: ./structor <input_file_name>
      // Generic error handler; see also catch(*)
      // Build a string 's' with desired detail; use this construct
      // througout such that 'catch(*)' is the only exception exit point
      // ex: "--Error-- ./structor: main: Process invoked without... etc..."
      //      std::string s = std::string(argv[0]) + std::string(": ")
      //	+ std::string(__FUNCTION__) + std::string(": ") 
      std::string s = whereami()
	+ std::string("Process invoked without input file name.")
	+ std::string("\n\tUsage: ") + std::string(std::string(argv[0]))
	+ std::string(" <file_name>\n");
      throw std::runtime_error(s.c_str());
    }

    // Reguired input file passed through command line 
    std::string s_fname = std::string(argv[1]);
    
    // Structor declaration
    // Initialize passive data members
    Structor_d2_i1 structor_d2_i1 = { // see Structor_d2_i1.h
      -1.0,  // d_01
      -2.0,  // d_02
      -3,    // i_01
    };

    // Open file for reading 
    std::fstream fin (s_fname.c_str(), std::ios::in);
    if (!fin) {
      // Generic error handler (see catch())
      // ex: "--Error-- ./structor: main: Required file not found: tmp.txt"
      std::string s = whereami()
	+ std::string("Required file not found: ") 
	+ std::string(s_fname);
      throw std::runtime_error(s.c_str());
    }

    // Optionally allocate container for placing the entire file in memory
    std::vector<Structor_d2_i1> v_structor_d2_i1;

    // Data specific structor load occurs in while()
    // This demo screen dumps the loaded values
    std::cout << "Contents of structor_d2_i1 records:\n";
    while (fin >> structor_d2_i1) { // load one record per pass until eof
      std::cout << "d_01: " << structor_d2_i1.d_01 << "\t";
      std::cout << "d_02: " << structor_d2_i1.d_02 << "\t";
      std::cout << "i_01: " << structor_d2_i1.i_01 << std::endl;

      v_structor_d2_i1.push_back(structor_d2_i1); // store record in container
    }
    std::cout << std::endl;

    // Screen dump container content
    std::cout << "Contents of v_structor_d2_i1 container:\n";
    std::vector<Structor_d2_i1>::iterator it;  
    int i = 0;
    for ( it = v_structor_d2_i1.begin();
	  it != v_structor_d2_i1.end(); 
	  it++, i++ ) {
      std::cout << "v_structor_d2_i1[" << i << "]: ";
      std::cout << it->d_01 << "\t";
      std::cout << it->d_02 << "\t";
      std::cout << it->i_01 << "\t";
      std::cout << "\n";
    }  // end for ( it =
    std::cout << std::endl;
    
    // Return status checking
    // Sanity checking the returned data is left to the user.
    // if (fin.eof()) std::cout << "--debug-- eof() reached" << std::endl; 
    if (!fin.eof()) {
      // Generic error handler (see catch())
      // ex: "--Error-- ./structor: main: File read ended ...; bad data: tmp.txt"
      std::string s = whereami()
	+ std::string("File read ended prematurely; data/structor mismatch\n") 
	+ std::string("\t  Check for invalid records.\n")
	+ std::string("\t  Check for extra content after last end of record\n")
	+ std::string("\t  Check for invalid endline char (e.g. dos format)\n")
	+ std::string("\t  Problematic data: ") + std::string(s_fname);
      throw std::runtime_error(s.c_str());
    } 

    exit (EXIT_SUCCESS);
    
  } // end try

  // Error handling
  catch(std::exception &e){
    std::cerr << "--Error-- " << e.what() << "\n" << std::endl;
    exit (EXIT_FAILURE);
  }
  catch(...){
    std::cerr << "--Error-- Undefined runtime error" << "\n" << std::endl;
    exit (EXIT_FAILURE);
  };
  
  
} // end  int main( int argc, char* argv[] ) 

// Notes:
/*
  Code lines tagged with --debug-- are intended for removal (commenting)
  in final release.

  See Structor_d2_i1.h and other include/Structor_*.h for other
  structor use cases.

 */
