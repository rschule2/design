// @file Structor_ui64_2.cpp (project structor-01)
// @date 2018.04.17
// @version (see git tags)
// @author schuler
// @info compile with: make

// @info Load a struct from file; file loader embedded in struct definition

// @info Adheres to c++98 standard... sorry. (tobify)
// @info Developers search '@dev' for usage notes.

// @info example output: Data set specific; load and display tabular file
/* 
   ./structor_ui64_2 Structor_ui64_2.txt
   02: 0xf00000000000
   ui64_01: 0x0	ui64_02: 0x143e0
   ui64_01: 0x1	ui64_02: 0xf00000000000
   
   Contents of v_structor_ui64_2 in memory:
   v_structor_ui64_2[0]: 0x0 Bytes: 8	0x143e0 Bytes: 8	
   v_structor_ui64_2[1]: 0x1 Bytes: 8	0xf00000000001 Bytes: 8	
   v_structor_ui64_2[2]: 0x2 Bytes: 8	0xa0000001b0000002 Bytes: 8	
   v_structor_ui64_2[3]: 0x0 Bytes: 8	0x143e0 Bytes: 8	
   v_structor_ui64_2[4]: 0x1 Bytes: 8	0xf00000000000 Bytes: 8	
   v_structor_ui64_2[5]: 0x0 Bytes: 8	0x143e0 Bytes: 8	
   v_structor_ui64_2[6]: 0x1 Bytes: 8	0xf00000000000 Bytes: 8
*/

/* 
   usage: ./structor_ui64_2 <infile> 
   see Structor_ui64_2.h and @notes
*/

#include <unistd.h>     // posix compliance
#include <iostream>     // stream io
#include <fstream>
#include <string>       // STL
#include <vector>
#include <cstdlib>      // EXIT_SUCCESS, EXIT_FAILURE
#include <stdexcept>    // std::runtime_error()
#include <iomanip>      // save/restore stream manipulators; copyfmt()

#include "Structor_ui64_2.h"
#include "structor.h"

int main( int argc, char* argv[] ) 
{
  try {
    // '__cplusplus' should report the c++ standard in use, but is broken 
    // on the obsolete TOB version of gcc/g++.  This bug was addressed in 2001.
    // On the TOB (Centos6/gcc 4.4.7), __cplusplus eroneously returns '1'.
    // 
    // std::cerr << __cplusplus << std::endl;  // show compiler standard used
    // // g++ -std=c++11 (or 14) for C++2011 (or 2014) to test

    if (argc != 2) { // usage: ./structor <input_file_name>
      // Generic error handler; see also catch(*)
      // Build a string 's' with desired detail; use this construct
      // througout such that 'catch(*)' is the only exception exit point
      // ex: "--Error-- ./structor: main: Process invoked without... etc..."
      std::string s = std::string(argv[0]) + std::string(": ")
	+ std::string(__FUNCTION__) + std::string(": ") 
	+ std::string("Process invoked without input file name.")
	+ std::string("\n\nUsage: ") + std::string(std::string(argv[0]))
	+ std::string(" <file_name>\n");
      throw std::runtime_error(s.c_str());
    }

    // Reguired input file passed through command line 
    std::string s_fname = std::string(argv[1]);

    // Structor declaration
    // Initialize passive data members; do *not* re-initialize nl:=newline char
    // 'nl' assignment may only occur in *.h definition
    Structor_ui64_2 structor_ui64_2 = {
      0xFFFFFFF1EEEEEEE1ULL,  // ui64_01
      0xFFFFFFF2EEEEEEE2ULL,  // ui64_02
    };

    // Open file for reading 
    std::fstream fin (s_fname.c_str(), std::ios::in);
    if (!fin) {
      // Generic error handler (see catch())
      // ex: "--Error-- ./structor: main: Required file not found: tmp.txt"
      std::string s = std::string(__FUNCTION__)
	+ std::string(": ")
	+ std::string("Required file not found: ") 
	+ std::string(s_fname);
      throw std::runtime_error(s.c_str());
    }

    // Optionally create a container when placing the entire file in memory
    // is desired.
    std::vector<Structor_ui64_2> v_structor_ui64_2;

    // Core struct loading block; target application specific; 
    // Simple screen dump in this demo.

    // load passives; note data are formated as hex in ascii file
    std::ios  sstate(NULL); // save stream format state
    sstate.copyfmt(std::cout);

    std::cout << "Contents of structor_ui64_2 loaded line by line:\n";
    while (fin >> structor_ui64_2) {
      std::cout << "ui64_01: " << std::hex << "0x" 
		<< structor_ui64_2.ui64_01 << "\t";
      std::cout << "ui64_02: " << std::hex << "0x" 
		<< structor_ui64_2.ui64_02 << "\n";

      // optionally maintain all data in memory
      v_structor_ui64_2.push_back(structor_ui64_2); 
    }
    std::cout << std::endl;
    std::cout.copyfmt(sstate); // replace stream format state

    // Dump the contents of vector container
    std::cout << "Contents of v_structor_ui64_2 in memory:\n";
    std::vector<Structor_ui64_2>::iterator it;  
    int i = 0;
    for ( it = v_structor_ui64_2.begin();
	  it != v_structor_ui64_2.end(); 
	  it++, i++ ) {
      std::cout << "v_structor_ui64_2[" << i << "]: ";
      std::cout << std::hex << "0x" << it->ui64_01 << " "
		<< "Bytes: " << sizeof(it->ui64_01) << "\t";
      std::cout << std::hex << "0x" << it->ui64_02 << " "
		<< "Bytes: " << sizeof(it->ui64_02) << "\t";
      std::cout << std::endl;
    }  // end for ( it =
    std::cout << std::endl;
    std::cout.copyfmt(sstate); // replace stream format state
    
    // Return status checking
    // Sanity checking the returned data is left to the user.
    // if (fin.eof()) std::cout << "--debug-- eof() OK" << std::endl; 
    if (!fin.eof()) {
      // Generic error handler (see catch())
      // ex: "--Error-- ./structor: main: File read ended ...; bad data: tmp.txt"
      std::string s = std::string(__FUNCTION__)
	+ std::string(": ")
	+ std::string("File read ended prematurely; data/structor mismatch\n") 
	+ std::string("  Check for invalid records.\n")
	+ std::string("  Check for extra data after last end of record char\n")
	+ std::string("  Check for invalid end of record char (e.g. dos format)\n")
	+ std::string("  Problematic data: ") + std::string(s_fname);
      throw std::runtime_error(s.c_str());
    } 
    exit (EXIT_SUCCESS);
    
  } // end try

  // Error handling
  catch(std::exception &e){
    std::cout << "--Error-- " << e.what() << "\n" << std::endl;
    exit (EXIT_FAILURE);
  }
  catch(...){
    std::cerr << "--Error-- Undefined runtime error" << "\n" << std::endl;
    exit (EXIT_FAILURE);
  };
  
  
} // end  int main( int argc, char* argv[] ) 

// Notes:
/*
  Code lines tagged with --debug-- are intended for removal (commenting)
  in final release.

  See minimal example Structor_d2_i1.cpp

 */
