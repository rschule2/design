// @file Structor_s1_ui32_6.cpp (project structor-01)
// @date 2018.04.16
// @version (see git tags)
// @author schuler
// @info compile with: make

// @info Load a struct from file; file loader embedded in struct definition

// @info Adheres to c++98 standard... sorry. (tobify)
// @info Developers search '@dev' for usage notes.

// @info example output: Data set specific; load and display tabular file
/* 
   ./structor_s1_ui32_6 Structor_s1_ui32_6.txt
   Contents of structor_s1_ui32_6 loaded line by line:
   s_01: V1ATT.BIN1	ui32_01: 144851 ...	ui32_06: 0x2744aa01
   s_01: V1ATT.BIN2	ui32_01: 144852 ...	ui32_06: 0x2744aa02
   s_01: V1ATT.BIN3	ui32_01: 144853 ...	ui32_06: 0x2744aa03
   
   Contents of v_structor_s1_ui32_6 in memory:
   v_structor_s1_ui32_6[0]: V1ATT.BIN1	144851 ...	0x2744aa01	
   v_structor_s1_ui32_6[1]: V1ATT.BIN2	144852 ...	0x2744aa02	
   v_structor_s1_ui32_6[2]: V1ATT.BIN3	144853 ...	0x2744aa03
*/

/* 
   usage: ./structor_s1_ui32_6 <infile> 
   see @notes
*/

#include <unistd.h>     // posix compliance
#include <iostream>     // stream io
#include <fstream>
#include <string>       // STL
#include <vector>
#include <cstdlib>      // EXIT_SUCCESS, EXIT_FAILURE
#include <stdexcept>    // std::runtime_error()
#include <iomanip>      // save/restore stream manipulators; copyfmt()

#include "Structor_s1_ui32_6.h"
#include "structor.h"

int main( int argc, char* argv[] ) 
{
  try {
    // '__cplusplus' should report the c++ standard in use, but is broken 
    // on the obsolete TOB version of gcc/g++.  This bug was addressed in 2001.
    // On the TOB (Centos6/gcc 4.4.7), __cplusplus eroneously returns '1'.
    // 
    // std::cerr << __cplusplus << std::endl;  // show compiler standard used
    // // g++ -std=c++11 (or 14) for C++2011 (or 2014) to test

    if (argc != 2) { // usage: ./structor <input_file_name>
      // Generic error handler; see also catch(*)
      // Build a string 's' with desired detail; use this construct
      // througout such that 'catch(*)' is the only exception exit point
      // ex: "--Error-- ./structor: main: Process invoked without... etc..."
      std::string s = std::string(argv[0]) + std::string(": ")
	+ std::string(__FUNCTION__) + std::string(": ") 
	+ std::string("Process invoked without input file name.")
	+ std::string("\n\nUsage: ") + std::string(std::string(argv[0]))
	+ std::string(" <file_name>\n");
      throw std::runtime_error(s.c_str());
    }

    // Reguired input file passed through command line 
    std::string s_fname = std::string(argv[1]);
    
    // Structor declaration
    // Initialize passive data members; do *not* re-initialize nl:=newline char
    // 'nl' assignment may only occur in *.h definition
    Structor_s1_ui32_6 structor_s1_ui32_6 = {
      "",  // s_01
      0xFFFFFFF1,  // ui32_01
      0xFFFFFFF2,  // ui32_02
      0xFFFFFFF3,  // ui32_03
      0xFFFFFFF4,  // ui32_04
      0xFFFFFFF5,  // ui32_05
      0xFFFFFFF6,  // ui32_06
    };

    // Open file for reading 
    std::fstream fin (s_fname.c_str(), std::ios::in);
    if (!fin) {
      // Generic error handler (see catch())
      // ex: "--Error-- ./structor: main: Required file not found: tmp.txt"
      std::string s = std::string(__FUNCTION__)
	+ std::string(": ")
	+ std::string("Required file not found: ") 
	+ std::string(s_fname);
      throw std::runtime_error(s.c_str());
    }

    // Optionally create a container when placing the entire file in memory
    // is desired.
    std::vector<Structor_s1_ui32_6> v_structor_s1_ui32_6;

    // Core struct loading block; target application specific; 
    // Simple screen dump in this demo.

    // load passives; note ui32_3-6 are formated as hex in ascii file
    std::ios  sstate(NULL); // save stream format state
    sstate.copyfmt(std::cout);

    // ui32_2-5 are omited in example display (the are loaded though)
    std::cout << "Contents of structor_s1_ui32_6 loaded line by line:\n";
    while (fin >> structor_s1_ui32_6) {
      std::cout << "s_01: " << structor_s1_ui32_6.s_01 << "\t";
      std::cout << "ui32_01: " << structor_s1_ui32_6.ui32_01 << " ...\t";
      std::cout << "ui32_06: " << std::hex << "0x" 
		<< structor_s1_ui32_6.ui32_06 << "\n";
      // optionally maintain all data in memory
      v_structor_s1_ui32_6.push_back(structor_s1_ui32_6); 
    }
    std::cout << std::endl;
    std::cout.copyfmt(sstate); // replace stream format state

    // Dump the contents of vector container
    // ui32_2-5 are omited in example display (the are loaded though)
    std::cout << "Contents of v_structor_s1_ui32_6 in memory:\n";
    std::vector<Structor_s1_ui32_6>::iterator it;  
    int i = 0;
    for ( it = v_structor_s1_ui32_6.begin();
	  it != v_structor_s1_ui32_6.end(); 
	  it++, i++ ) {
      std::cout << "v_structor_s1_ui32_6[" << i << "]: ";
      std::cout << it->s_01 << "\t";
      std::cout << it->ui32_01 << " ...\t";
      std::cout << std::hex << "0x" << it->ui32_06 << "\t";
      std::cout << std::endl;
    }  // end for ( it =
    std::cout << std::endl;
    std::cout.copyfmt(sstate); // replace stream format state
    
    // Return status checking
    // Sanity checking the returned data is left to the user.
    // if (fin.eof()) std::cout << "--debug-- eof() OK" << std::endl; 
    if (!fin.eof()) {
      // Generic error handler (see catch())
      // ex: "--Error-- ./structor: main: File read ended ...; bad data: tmp.txt"
      std::string s = std::string(__FUNCTION__)
	+ std::string(": ")
	+ std::string("File read ended prematurely; data/structor mismatch\n") 
	+ std::string("  Check for invalid records.\n")
	+ std::string("  Check for extra data after last end of record char\n")
	+ std::string("  Check for invalid end of record char (e.g. dos format)\n")
	+ std::string("  Problematic data: ") + std::string(s_fname);
      throw std::runtime_error(s.c_str());
    } 

    exit (EXIT_SUCCESS);
    
  } // end try

  // Error handling
  catch(std::exception &e){
    std::cout << "--Error-- " << e.what() << "\n" << std::endl;
    exit (EXIT_FAILURE);
  }
  catch(...){
    std::cerr << "--Error-- Undefined runtime error" << "\n" << std::endl;
    exit (EXIT_FAILURE);
  };
  
  
} // end  int main( int argc, char* argv[] ) 

// Notes:
/*
  Code lines tagged with --debug-- are intended for removal (commenting)
  in final release.

  See minimal example Structor_d2_i1.cpp

 */
