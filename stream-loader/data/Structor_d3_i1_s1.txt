# Structor_d3_i1_s1.txt; Comment lines are optional
# Address fail when std::string comprises last field of record
# Require trailing delimiter in this case, e.g. ';'
1.1, 1.2, 1.3, 4, "test1";
1.1, 1.2, 1.3, 4, 25mA;
