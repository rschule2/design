@file minimal_structor/README
@date 2018.04.18
@author schuler
@info minimal use case for structor

--------------------------------------------------------------------------------
Project 'minimal_structor': create a bare minimum use case for a structor
ascii file to struct loader. Links libstructor.a; structor src not required.
--------------------------------------------------------------------------------

@date 2018.04.18 rcs project start
@date 2018.04.25 rcs added end of record (eor; \n') requirement

--------------------------------------------------------------------------------
A 'structor' is a generic method for reading delimited ascii data from 
files into a struct or class.  The combined passive data set and loading 
operator '>>" is called a 'structor'.

The following standard data structure 'StandardStruct_d2_i1' is replaced
with the structor 'Structor_d2_i1'.  The operator overload of ">>"
provides the means for loading a delimited ascii files dataset into
the structor.

struct StandardStruct_d2_i1 {  // non structor container
  double d_01;
  double d_02;
  int    i_01;
}; 

struct Structor_d2_i1 {        // structor container

  double d_01; 
  double d_02;
  int    i_01;

  friend std::iostream & operator >>(std::iostream & in, 
				     Structor_d2_i1 & p) {
    in >> extendwhite 
       >> p.d_01 >> p.d_02 >> p.i_01 >> eol;

    return in;
  }
}; 

Assuming 'fin' is file stream open for reading, structor may be
loaded with a loop construct such as:

while (fin >> structor_d2_i1) { ...)


--------------------------------------------------------------------------------
File Descriptions:
--------------------------------------------------------------------------------

./minimal_structor                           // project directory
./README
./makefile
./src/Structor_d2_i1.cpp	             // minimal structor use case
./include/Structor_d2_i1.h
./Structor_d2_i1.txt                         // simple 7 record data set

--------------------------------------------------------------------------------
@notes:
--------------------------------------------------------------------------------

/*
- Code lines tagged with --debug-- are intended for removal (commenting)
  in final release.
- Adheres to c++98 standard... sorry. (tobify).
- The code will be greatly simplified  when the tob is upgraded to c++11/14
  50 years from now.

*/
