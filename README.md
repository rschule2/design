------------------------------------
Hardware and Software Design Samples
------------------------------------

The files in this repository provide samples of my work.

Use the 'source' files interface to view 'stream-loader', 'ovtile4', and 'Style Transfer Images', described in the following three paragraphs.

'stream-loader' is a C/C++ generic ascii file loader.  I define the concept of a 'structor' which includes passive data objects, as well as overloaded stream functions for file i/o.  See stream-loader/README.md for further details.

'ovtile4_3' is a python function for splitting image files into 4 tiles, allowing neural network style transfer on large images. This function is part of a larger application producing style transfer on large files (1920x1080) using TensorFlow.  The kernel size of most trained conv nets are typically small (400px on a side (2018)).  Image style transfer works best on similarly sized 'content' and 'style' images.  In addition, the GPU and memory requirements to process images much bigger than ~800px on a side are beyond my hobby budget :)  See ovtile4/README.md for further details.

The folder 'Style Transfer Images' contains output files created with the application described above.  These images were processed at 1/16th their final size with 'ovtile4_3', and stitched together with inverse code called 'ovjoin4_3'.

The files referenced in 'Design Communication Samples' show typical documents I have created.  I believe a well documented project or product has a higher likelihood of success.

-- Ray Schuler

----------------------------
Design Communication Samples
----------------------------
I include documents for Analog building blocks, and a completed product IC. The former provides voltage conversion sub-components for an RF antenna switch controller found on mobile devices with multiple transceivers sharing an antenna-- WiFi, LTE, etc.  The latter comprises the Customer Application Note for an RS-485 transceiver available through an OEM IC parts catalog.

The first step of the design process is the communication of Objective Specifications.  The file [fem-sw-design-presentation-20150911a-public.pdf](https://bitbucket.org/rschule2/design/src/master/fem-sw-design-presentation-20150911a-public.pdf) provides a high level overview of the proposed project; the presentation is intended to ensure customer needs are met within an acceptable level of resource expenditure.

Oftentimes documents are required to address questions about  specific features. The file [hv-front-end-vs-bootstrap-buck-schuler-20111018.pdf](https://bitbucket.org/rschule2/design/src/master/hv-front-end-vs-bootstrap-buck-schuler-20111018.pdf] describes a technique called 'bootstrapping', which allows extending the input voltage range for a given technology.

Any design subcomponent, hardware or software, is tested at the unit level to ensure the success of the aggregate design.  The file [lv_bgr2b_lab_char_report_20110406a.pdf](https://bitbucket.org/rschule2/design/src/master/lv_bgr2b_lab_char_report_20110406a.pdf) provides a characterization report for a CMOS current mode bandgap reference, critical to the performance of most commercial electronic devices.

The file [scp-application-note-20160915a-public.pdf](https://bitbucket.org/rschule2/design/src/master/scp-application-note-20160915a-public.pdf) is a Customer Application Note detailing the design theory and schematics for customers wishing to include these circuits in their own IC designs. 

The file [LTMag-V16N02-07-LTC2859-Schuler.pdf](https://bitbucket.org/rschule2/design/src/master/LTMag-V16N02-07-LTC2859-Schuler.pdf) is a Customer Application Note providing theory and performance detail for customers wishing to include the packaged OEM IC into their system designs.


-----------
Downloading
-----------

Clone this repository to your system with:
git clone https://bitbucket.org/rschule2/design.git

Or use the interface at [https://bitbucket.org/rschule2/design](https://bitbucket.org/rschule2/design) to download specific files.  Download binary files by selecting 'View raw' when prompted.
