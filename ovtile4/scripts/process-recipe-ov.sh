#! /bin/bash

# python image seam removal experiments - overlap process

# 2018.05.01 rcs

# Comment and move '--width ${CONTENT_WIDTH} \' to use original size

DIR="../20180501-recipe"
CONTENT="may-garlic-pot-c.jpg"
WIDTH=960
OVERLAP=40

OUT="recipe-ov"

overlapx2() {
    python ./overlapx2.py \
	   --content ${DIR}/${CONTENT} \
	   --overlap ${OVERLAP} \
    	   --width ${WIDTH} \
	   --output_base ${DIR}/${OUT}

} # end ns_tf

overlapx2

