#! /bin/bash

# parameterzied neural style script (see --config--) for seamx2 process
# schuler 2018.05.01

#   lr: 2-8 seem best; above 10 very noisy; move default from 10->6?
#   bw: 0.05 doesn't do much; 0.1-1 seems best; 0.8 is pretty abstract

# --config--
# manually '--preserve colors' if desired
# namually remove '-width' for original content size
# change 'APP' to "." when running locally

DIR="../20180501-recipe"
NAMETAG="recipe"
CONTENT_LEFT=${DIR}/"recipe-ov_left.jpg"
CONTENT_RIGHT=${DIR}/"recipe-ov_right.jpg"
STYLE=${DIR}/"style-may-recipe-b.jpg"
WIDTH=520 # ignored if commented below (520)
ITER=2000
STYLE_SCALE=1.4

declare -a b1=(0.9)   # beta1
declare -a b2=(0.999) # beta2
declare -a bw=(0.1)   # style-blend-weights
declare -a sw=(500)   # style-weight
declare -a cw=(2)     # content-weight
declare -a lwe=(2.0)  # style-layer-weight-exp
declare -a tv=(40)    # total-variation-weight 
declare -a lr=(8)     # learning-rate

# strings: declare -a arr=( "a" "b" )
#          for i in "${arr[@]}"

APP="/content/drive/app"  # google
# APP="."  # local

ns_tf() {
    
    B1=${7}  # beta 1
    B2=${8}  # beta 2
    BW=${1}  # style-blend-weights
    SW=${5}  # style-weight
    CW=${4}  # content-weight
    LWE=${3} # style-layer-weight-exp
    LR=${2}  # learning-rate
    TV=${6}  # total-variation-weight 
    
    OUT=`echo "${NAMETAG}-B${BW}L${LR}E${LWE}C${CW}S${SW}T${TV}B${B1}B${B2}" | sed "s/\./p/g"`
    
    python neural_style_tile.py \
	--content_left ${DIR}/${CONTENT_LEFT} \
	--content_right ${DIR}/${CONTENT_RIGHT} \
	--styles ${DIR}/${STYLE} \
	--output_base ${APP}/${OUT} \
	--print-iterations 200 \
	--checkpoint-output ${APP}/${OUT}-%s.jpg \
	--checkpoint-iterations 200 \
	--iterations ${ITER}  \
	--style-weight ${SW} \
	--content-weight ${CW} \
	--tv-weight ${TV} \
	--style-blend-weights ${BW} \
	--style-layer-weight-exp ${LWE} \
	--learning-rate ${LR} \
	--beta1 ${B1} \
	--beta2 ${B2} \
	--style-scale ${STYLE_SCALE}
    
#       --preserve-colors \
#	--width ${WIDTH} \

    
} # end  ns_tf()

for ilr in ${lr[@]}
do
    for itv in ${tv[@]}
    do
	for ilwe in ${lwe[@]}
	do
	    for icw in ${cw[@]}
	    do
		for isw in ${sw[@]}
		do
		    for ibw in ${bw[@]}
		    do
			for ib1 in ${b1[@]}
			do
			    for ib2 in ${b2[@]}
			    do
				echo beta1: $ib1
				echo beta2: $ib2
				echo style-blend-weights: $ibw
				echo learning-rate: $ilr
				echo style-layer-weight-exp: $ilwe
				echo content-weight: $icw
				echo style-weight: $isw
				echo total-variation-weight: $itv
				ns_tf $ibw $ilr $ilwe $icw $isw $itv $ib1 $ib2
			    done
			done
		    done
		done
	    done
	done
    done
done

# notes
# --preserve-colors \

# default arguments
# CONTENT_WEIGHT = 5e0
# CONTENT_WEIGHT_BLEND = 1
# STYLE_WEIGHT = 5e2
# TV_WEIGHT = 1e2
# STYLE_LAYER_WEIGHT_EXP = 1
# LEARNING_RATE = 1e1
# BETA1 = 0.9
# BETA2 = 0.999
# EPSILON = 1e-08
# STYLE_SCALE = 1.0
# ITERATIONS = 1000
# VGG_PATH = 'imagenet-vgg-verydeep-19.mat'
# POOLING = 'max'

# usage: neural_style.py [-h] 
# --content CONTENT --styles STYLE [STYLE ...]
# --output OUTPUT [--iterations ITERATIONS]
# [--print-iterations PRINT_ITERATIONS]
# [--checkpoint-output OUTPUT]
# [--checkpoint-iterations CHECKPOINT_ITERATIONS]
# [--width WIDTH]
# [--style-scales STYLE_SCALE [STYLE_SCALE ...]]
# [--network VGG_PATH]
# [--content-weight-blend CONTENT_WEIGHT_BLEND]
# [--content-weight CONTENT_WEIGHT]
# [--style-weight STYLE_WEIGHT]
# [--style-layer-weight-exp STYLE_LAYER_WEIGHT_EXP] 
#    0.2==fine, 2.0==coarse
# [--style-blend-weights STYLE_BLEND_WEIGHT [STYLE_BLEND_WEIGHT ...]]
#    1.0==default, 0.1==abstract
# [--tv-weight TV_WEIGHT] [--learning-rate LEARNING_RATE]
# [--beta1 BETA1] [--beta2 BETA2] [--eps EPSILON]
# [--initial INITIAL]
# [--initial-noiseblend INITIAL_NOISEBLEND]
# [--preserve-colors] [--pooling POOLING]

