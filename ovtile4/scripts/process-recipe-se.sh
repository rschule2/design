#! /bin/bash

# python image seam removal experiments

# 2018.05.01 rcs


DIR="../20180501-recipe"
CONTENT_LEFT=${DIR}/"recipe2-B0p1L8E2p0C2S500T40B0p9B0p999-left.jpg"
CONTENT_RIGHT=${DIR}/"recipe2-B0p1L8E2p0C2S500T40B0p9B0p999-right.jpg"
OVERLAP=40

OUT=${DIR}/"recipe2-se"

seamx2() {
    python ./seamx2.py \
	   --content_left ${DIR}/${CONTENT_LEFT} \
	   --content_right ${DIR}/${CONTENT_RIGHT} \
	   --overlap ${OVERLAP} \
	   --output ${OUT}.jpg
    
} # end ns_tf

seamx2




