#! /bin/bash

# testbench calling script for ovtile4

# 2018.05.02 rcs
# 2018.05.11 rcs python3 termux mods
# 2018.05.12 rcs added 'quiet' flag

# --todo-- add 'rnd_pct' support

DIR="."
CONTENT="seam-testimage.jpg"
NEW_WIDTH=960
OUT_BASE="ovt"
OVERLAP=100
QUIET=1

OUT="ovtile4-out"  # use as output base, e.g. ovtile4-out-q1.jpg, etc..

ovtile4() {
    python ./run_ovtile4.py \
	   --content ${DIR}/${CONTENT} \
	   --new_width ${NEW_WIDTH} \
	   --output_base ${OUT_BASE} \
	   --overlap ${OVERLAP} \
	   --quiet ${QUIET}

} # end ovtile4

ovtile4

