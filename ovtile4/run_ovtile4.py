#!/usr/bin/env python

# @file run_ovtile4.py
# @info run script ovtile4 quadrant image splitter, with overlap
# @info Intended for use in high res neural style imaging

# @date 2018.05.02 rcs start
# @date 2018.05.12 rcs add 'quiet' flag

import os
from sys import exit

from ovtile4_3 import ovtile4

import numpy as np
import scipy.misc
import math
from argparse import ArgumentParser
from PIL import Image

def build_parser():
    parser = ArgumentParser(description='Image divider with overlap.')
    parser.add_argument('--content',
            dest='content', help='content image',
            metavar='CONTENT', required=True)
    parser.add_argument('--output_base',
            dest='output_base', help='output base name; -q1, (2,3,4)added',
            metavar='CONTENT_BASE', required=True)
    parser.add_argument('--new_width', type=int,
            nargs='?', const=0,
            dest='new_width', 
            help='content_image resize width in px (default==0; no resize)',
            metavar='new_width', required=False)
    parser.add_argument('--overlap', type=int,
            nargs='?', const=0, default=0,
            dest='overlap', 
            help='per side overlap in px (default==0)',
            metavar='OVERLAP', required=False)
    parser.add_argument('--quiet', type=int,
            nargs='?', const=0, default=0,
            dest='quiet', 
                        help='1==quiet, 0==verbose',
            metavar='QUIET', required=False)

    return parser

def main():

    # grab program args
    parser = build_parser()
    options = parser.parse_args()

    content_image_name = options.content

    CONTENT_IMAGE = imread(content_image_name)
    NEW_WIDTH = options.new_width
    OV = options.overlap
    QUIET = options.quiet

    mq1, mq2, mq3, mq4 = \
            ovtile4(CONTENT_IMAGE, NEW_WIDTH,
                    OV, QUIET)

    print ("Quadrant 1-4 shapes: " )
    print (mq1.shape)
    print (mq2.shape)
    print (mq3.shape)
    print (mq4.shape)

    imsave( options.output_base+'-q1.jpg', mq1)
    imsave( options.output_base+'-q2.jpg', mq2)
    imsave( options.output_base+'-q3.jpg', mq3)
    imsave( options.output_base+'-q4.jpg', mq4)

def imread(path):
    img = scipy.misc.imread(path).astype(np.float)
    if len(img.shape) == 2:
        # grayscale
        img = np.dstack((img,img,img))
    elif img.shape[2] == 4:
        # PNG with alpha channel
        img = img[:,:,:3]
    return img

def imsave(path, img):
    img = np.clip(img, 0, 255).astype(np.uint8)
    Image.fromarray(img).save(path, quality=95)

if __name__ == '__main__':
    main()
