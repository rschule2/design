2018.05.02

ovtile4_3: Function for splitting image into four quadrants, with 
	 parameterized overlap; optional overlap randomness

Prerequisites:

- After Python (2.7 or 3) and Pip are installed:
  python -m pip install --user numpy scipy Pillow

--------------------------------------------------------------------------------
Note: This version of 'ovtile4_3' is for use a coding sample only.
This project normaly controlled by an encryped git repo. The .git
directory, with historical commits, has been removed for this sample code.
-- Ray Schuler 2018.08.01
--------------------------------------------------------------------------------

2018.05.02 rcs start (from prototype overlap2x)

Pyton 'ovtile4_3.py' and 'ovjoin4_3.py' were designed to perform conv net
image style transfer on very large images.  These function 'tile'
and 'join' the input images prior to processing in TensorFlow.

Style transfer is a spacial process, thus the edges of tile(n) will
not exactly match the abutting edige of tile(n+1).  To handle this,
a parametric overlap is coded into ovtile and ovjoin.

This code is run recursively, such that running 'ovtile4' on
the output the quadrants previously produced by 'ovtile4' breaks the
input image into 16 tiles.  16 tiles produces good results on an HD
image (1920 x 1080) on a mid-range GPU and moderate memory space.

A stand alone test progam is included:
. ./process-ovtile4.sh

Follow this script for the appropriate setup and usage of ovtile5.py.

The test program produces the following files (4 quadrants of the imput image)

ovt-q1.jpg
ovt-q2.jpg
ovt-q3.jpg
ovt-q4.jpg


