#!/usr/bin/env python

# @file ovtile4_3.py (python3 version for termux)
# @info Function 'ovtile4' quadrant image splitter, with overlap
# @info Intended for use in high res neural style imaging

# @date 2018.05.02 rcs start
# @date 2018.05.11 rcs python3: change print "..." to ("..."), '+' to ',"
# @date 2018.05.12 rcs added 'quiet' flag

# --todo-- make 'quiet' optional
# --todo-- make 'overand' a negative only bias

import os
from sys import exit

import numpy as np
import scipy.misc
import math

from random import randint
from PIL import Image

def ovtile4(content_image, new_width, ov, quiet):
    """
    Divide 'content_image' into four quadrant overlaping tiles.

    Overlaps are intended to be blended out in the final image.
    These files are joined by the inverse function ovjoin4().
    If 'new_width' is None, quadrants are created without image size mod,
    else 'content_image' is scaled to 'new_width' first.

    This function returns PIL images q1, q2, q3, q4, the divided quadrant tiles.
    Args:
      content_image : image to be divided in 4 with overlap
      new_width     : scale image width before division; None for no resize
      ov            : overlap in px
      quiet         : 1 == quiet; 0== verbose

    :rtype: tuple[image,image,image,image]
    """

    if new_width is None: # do not resize image
        width = content_image.shape[1]
        if quiet==0:
            print ("Content image width: " + str(width))
    else: # resize the image; maintain aspect ratio
        width = new_width
        new_shape = (int(math.floor(float(content_image.shape[0]) /
                                content_image.shape[1] * width)), width)
        content_image = scipy.misc.imresize(content_image, new_shape)
        if quiet==0:
            print ("Resized content image width: " + str(width))

    height = content_image.shape[0]
    if quiet==0:
        print ("Content image height: " + str(height))

    ov = 0 if (ov==None) else ov
    if quiet==0:
        print ("Tile overlap: " + str(ov))

    # define x terminal coordiates for tiles
    q1ws = q4ws = int((width/2)-1 - ov)      # quadrant 1, 4 width start
    # print ("--debug-- Q1,Q4 width start  : " + str(q1ws))
    q2ws = q3ws = 0;                    # quadrant 2, 3 width start
    # print ("--debug-- Q2,Q3 width start  : " + str(q2ws))

    q1we = q4we = int(width-1)               # quadrant 1, 4 width end
    # print ("--debug-- Q1,Q4 width end    : " + str(q1we))
    q2we = q3we = int((width/2) + ov)       # quadrant 2, 3 width end
    # print ("--debug-- Q2,Q3 width end    : " + str(q2we))

    # define y terminal coordiates for tiles
    q1hs = q2hs = 0                     # quadrant 1, 2 height start
    # print ("--debug-- Q1,Q2 height start : " + str(q1hs))
    q3hs = q4hs = int((height/2)-1 - ov)     # quadrant 3, 4 height start
    # print ("--debug-- Q3,Q4 height start : " + str(q3hs))
    q1he = q2he = int((height/2) + ov)       # quadrant 1, 2 height end
    # print ("--debug-- Q1,Q2 height end   : " + str(q1he))
    q3he = q4he = int(height-1)              # quadrant 3, 4 height end
    # print ("--debug-- Q3,Q4 height end   : " + str(q3he))

    q1 = content_image[q1hs:q1he, q1ws:q1we, :]
    q2 = content_image[q2hs:q2he, q2ws:q2we, :]
    q3 = content_image[q3hs:q3he, q3ws:q3we, :]
    q4 = content_image[q4hs:q4he, q4ws:q4we, :]

    return  q1, q2, q3, q4  # PIL image quadrants

def main():

    # see test_ovtile4 for parsing testbench
    # def ovtile4(content_image, ov, rnd_pct):

    content_image_name = './seam-testimage.jpg'

    CONTENT_IMAGE = imread(content_image_name)
    NEW_WIDTH = None        # no resize
    # NEW_WIDTH = 600       # resize
    OV = 0
    QUIET=0

    mq1, mq2, mq3, mq4 = \
            ovtile4(CONTENT_IMAGE, NEW_WIDTH,
                    OV, QUIET)

    print ("Quadrant 1-4 shapes: ") 
    print (mq1.shape)
    print (mq2.shape)
    print (mq3.shape)
    print (mq4.shape)

    imsave( './q1.jpg', mq1)
    imsave( './q2.jpg', mq2)
    imsave( './q3.jpg', mq3)
    imsave( './q4.jpg', mq4)

    # test code for 'ovrand() to be used in ovjoin4()'
    RND_PCT = 10
    print ("'overand' test: ov, rnd_pct, overlap: " \
           + str(OV) + ", " + str(RND_PCT) + ", " \
           + str(ovrand(OV, RND_PCT)))
    

# Function ovrand() for use in ovjoin4()
# Adding randomness to the tiling function would require passing
# the randomized overlap to join; prefer to use common profile value
def ovrand(ov, rnd_pct):
    """
    GENERATE49 a random offset ov-10% <= ovrand <= ov+10%
    Min randomization is 1 px.  e.g. 9 <= ovrand(10,1) <= 11

    Returns int(ovrand) as defined above.
    Args:
      ov : overlap in px
      rnd_pct : percent randomness.  e.g. rnd_pct==10 yields an overlap
                of ov +/- 10%

    :rtype: int
    """
    if ov==0:
        return 0
    max_offset = max((ov*rnd_pct*0.01), 1)
    ovrand = ov + math.floor(float(randint(0, 2*max_offset)) - max_offset)
    # --note-- random.randint(1, 10) # Integer 1 to 10, endpoints included

    return math.floor(ovrand)


def imread(path):
    img = scipy.misc.imread(path).astype(np.float)
    if len(img.shape) == 2:
        # grayscale
        img = np.dstack((img,img,img))
    elif img.shape[2] == 4:
        # PNG with alpha channel
        img = img[:,:,:3]
    return img

def imsave(path, img):
    img = np.clip(img, 0, 255).astype(np.uint8)
    Image.fromarray(img).save(path, quality=95)

if __name__ == '__main__':
    main()
